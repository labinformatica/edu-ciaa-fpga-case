# edu-ciaa-fpga-case

En este sitio podrá encontrar un diseño básico de un gabinete para la [EDU-CIAA-FPGA](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo%3Aedu-fpga). Está diseñado a fin de poder ser impreso utilizando impresoras 3d FDM y consta solo de dos partes ([base](case_r03_base.stl) y [tapa](case_r03_tapa.stl)) . 

Entre las características del diseño puede mensionarse:
- Teclas con extensores a fin de facilitar la interacción con los pulsadores incluídos en la placa.
- Sobre cada led se ha incluído una pequeña perforación a fin de facilitar la identificación del estado de los mismos. 
- Ha sido diseñado a fin de mantener juntas las partes por presión (Cuidado, una vez unidas las partes su separación debe realizarce cuidadosamente a fin de no dañar la placa.)

# Algunas imágenes

Captura del gabinete en FreeCAD

![Render en FreeCad](case_03_img.png)

Gabinte impreso y en funcionamiento. Note el detalle de los leds. Según el color del material utilizado para realizar la impresión puede notarse en mayor o menor medida su brillo. 

![Gabinete funcionando](foto_02.jpg)

